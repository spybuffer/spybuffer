# tptest package.
# Python code for tp-fw testbench goes here.
# setup script installs this in "editable" mode into
# a virtualenv. This avoids having to set PYTHONPATH.

# editable mode means that a symlink to the package is
# installed rather than a copy; so changes here will
# automatically take effect.
