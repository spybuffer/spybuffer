import cocotb
from cocotb import triggers, result

from . import driver, monitor, events, util


class DataflowController(object):

    def __init__(self, dut, clock, input_width=65, input_zp=0):
        self.dut = dut
        self.clock = clock
        self.input_width = input_width
        self.input_zp    = input_zp

        self.input_fifos = {}
        self.output_fifos = {}

        # We should maybe give these names?
        self.empty_blocks = {}

        # Store a record of the events which were sent for each input.
        # Currently, this isn't actually used.
        self.input_log = {}
    
       
    # TODO: guards should be added here to check that the paths are _actually_ FIFOs.

    def add_input_fifo(self, name, path):
        """ Add an input FIFO driver. Returns reference to the object."""
        if name not in self.input_fifos:
            # Create the FIFO driver object; it starts automatically.
            self.input_fifos[name] = driver.FifoDriver(path, self.clock)
            self.input_log[name] = []
        return self.input_fifos[name]

    def add_output_fifo(self, name, path, output_file=None):
        """ Add an output FIFO monitor. Returns reference to the object."""
        if name not in self.output_fifos:
            # Create the FIFO monitor object; it also starts automatically.
            self.output_fifos[name] = monitor.FifoMonitor(path, self.clock, output_file)
        return self.output_fifos[name]

    def stop(self):
        """ Stop all coroutines. Not sure this is necessary."""
        for driver in self.input_fifos.values():
            driver.kill()

        for monitor in self.output_fifos.values():
            monitor.kill()

            # Closes connected output file (if one was configured).
            if monitor.output_file != None:
                monitor.output_file.close()

        for empty_block in self.empty_blocks.values():
            empty_block.kill()

    def start(self):
        """ Start any coroutines not automatically started."""
        # I had a coroutine that would try to run and catch errors.
        # However, it didn't seem to be working properly, so I decided
        # (especially for this, spy-buffer-only version of the tests) the
        # simpler thing to do was just remove it. That means we might not be
        # able to close binary files properly in the case of a crash, but...
        # these tests don't use that functionality anyway. So all in all,
        # I think this is okay.

        # If additional coroutines _do_ need to be added, they could be
        # started in this function.
        pass

    def send_event(self, event, name, expect_fail=False):
        """ Sends an event to input 'name'. Does not block."""
        # Store the L0ID that we're sending. Also dispatch it to the monitors.
        self.input_log[name].append(event.l0id)
        

        if not expect_fail:
            for output_fifo in self.output_fifos.values():
                output_fifo.expected_ids.add(event.l0id)

        # Send it!
        words = list(event)

        for word in words[:-1]:            
            self.input_fifos[name].append(word.get_binary(zp=self.input_zp, size=self.input_width-1))            
        else:
            hook = triggers.Event()
            self.input_fifos[name].append(words[-1].get_binary(), event=hook)

        # Log once an event was sent.
        self.dut._log.info("Dataflow sent full event (L0ID " + util.hex(event.l0id) + ") to FIFO '" + name + "' with " + str(len(event)) + " words.")

        return hook

    @cocotb.coroutine
    def send_sync_event(self, event, name, expect_fail=False):
        """ Sends an event to input 'name'. Blocks until event is sent."""
        # Actually, how can this work?
        hook = self.send_event(event, name, expect_fail=expect_fail)
        yield hook.wait()

    def send_events_from_file(self, filename, name, expect_fail=False):
        """ Reads an event file (filename) and sends events to input 'name'.
            Returns hook which can be yielded."""
        input_events = events.read_events_from_file(filename)
        if len(input_events) != 0:
            for event in input_events:
                hook = self.send_event(event, name, expect_fail=expect_fail)
            else:
                return hook
        return None

    def send_random_events(self, name, n_random, expect_fail=False):
        """ Sends n_random random events to input 'name'.
            Returns hook which can be yielded."""
        random_events = events.get_random_events(n_random)
        for event in random_events:
            hook = self.send_event(event, name, expect_fail=expect_fail)
        else:
            return hook, random_events

    @cocotb.coroutine
    def check_outstanding(self, timeout=-1, units='ns'):
        """ Coroutine that blocks until there are no outstanding events anywhere."""

        #done = True
        hooks = []
        for output_fifo in self.output_fifos.values():
            # We can't exit if there are still events.
            # If there are still events: tell the output FIFO to fire a trigger
            # once there are no more.
            if len(output_fifo.expected_ids) != 0:
                output_fifo.on_empty.clear()
                hooks.append(output_fifo.on_empty.wait())
                output_fifo.expect_empty = True
                done = False
            else:
                done = True
            

        # If there are still pending events:
        # Use the with_timeout() + combine triggers.
        # Exit either on timeout or after all the combined triggers fire.
        if not done:
            self.dut._log.info("Waiting for outstanding events...")
            tr = triggers.Combine(*hooks)
            if timeout == -1:
                yield tr
            else:
                yield triggers.with_timeout(tr, timeout, units)

    @cocotb.coroutine
    def wait_for_errors(self):
        """ Coroutine that waits for a driver or monitor to fail."""
        errors = []

        # There must be a cleaner way to iterate through both at the same time.
        for name, fifo in input_fifos.items():
            if fifo.on_error is not None:
                errors.append(fifo.on_error.wait())
        for name, fifo in output_fifos.items():
            if fifo.on_error is not None:
                errors.append(fifo.on_error.wait())

        # Yield the errors list!
        # This will resume only when one of the fifo on_error events fires.
        on_error = yield errors

        # This coroutine is forked. If we _ever_ get this far, it means something
        # has gone badly wrong (i.e. the test will fail).

        # Cleanup stage: call self.stop().
        # This should automatically force output files to close.
        self.stop()

        # This could be configured to allow other things to happen. e.g., continue after
        # failure but perform a reset.

        # Fail the test! Print out relevant message.
        raise result.TestFailure("Error: failure from FIFO: " + str(on_error.data))
