# This module interfaces between cocotb and the dataflow code
# in the system-simulation repository, which knows about things like
# data formats.

# This code NO LONGER depends on the DataFormat package. It makes the
# following assumptions:
# * Event words begin with a metadata bit, followed by a flag of given
# length that indicates event headers and footers.
# * The LSBs of the header word represent a form of "event ID", referred
# to here as the "L0ID".
# The lengths and header/footer values are set in constants.py.

import random
import os
import struct

from . import constants
from . import util

class DataWord(object):

    def __init__(self, contents, is_metadata=False, do_parse=True):
        self.contents = contents
        self.is_metadata = is_metadata

        # Flag. Only assigned if this is a metadata word.
        self.flag = None

        # in principle, this could be called by other routines.
        if do_parse:
            self._parse()

    def __str__(self):
        return str(self.get_binary())

    def __repr__(self):
        return str(self.get_binary())

    def _parse(self):
        """ Parses a data word from binary representation."""
        if self.is_metadata:
            # The flag should be the uppermost flag_size bits.
            # We can retrieve this with a bit of binary arithmetic.
            bitmask = 2**(constants.word_length - constants.flag_size) * 0xff
            self.flag = (self.contents & bitmask) >> (constants.word_length - constants.flag_size)

        # TODO: we could do other parsing here!
        # Collaborate with Elliot to decide if we want to extract more info.

    def get_binary(self, zp = 0, size=64):
        """ Returns binary value representation of this word, with is_metadata as the MSB."""
        full_value = util.BinaryValue(self.contents, n_bits=size+1)
        full_value[size-zp] = int(self.is_metadata)        
        return full_value

    # these are helper functions. They're not _strictly_ necessary but... useful?
    def is_start_of_event(self):
        """ Checks if the word is a start-of-event metadata word."""
        if self.flag == constants.EVT_HDR_FLAG:
            return True
        return False

    def is_end_of_event(self):
        if self.flag == constants.EVT_FTR_FLAG:
            return True
        return False

    def write(self, output, endian='little'):
        """ Saves the word to a writable object (e.g. binary data file)."""
        format_string = "<?Q"
        if endian != 'little':
            format_string = ">?Q"
        output.write(struct.pack(format_string, self.is_metadata, self.contents))

   

class DataEvent(object):

    # Does this class need to do anything else?
    # I'm not actually sure it does.

    def __init__(self, l0id):
        self.words = []
        self.l0id = l0id

    def add_word(self, word):
        self.words.append(word)

    def write(self, output, endian='little'):
        """ Writes the event to a writable object by saving every word."""
        for word in words:
            word.write(output, endian)

    def __str__(self):
        return "DataEvent (L0ID " + str(l0id) + "), " + str(len(self.words)) + " words"

    def __repr__(self):
        return self.__str__()

    def __len__(self):
        return len(self.words)

    def __iter__(self):
        return iter(self.words)

def get_l0id_from_event(word):
    """ Helper function that uses the DataFormat package to get the L0ID from a word.
        If the word is not a start-of-event word, it returns -1."""
    if word.is_start_of_event():
        # This should be the lower 40 bits. Again, can retrieve with binary arithmetic.
        return word.contents & (2**constants.l0id_size - 1)
    return -1

# Adapted from event reader here in HTT DataFormat code.
# We don't really use this functionality in the testbench anymore, but it still exists.
def read_events_from_file(filename, endian='little'):
    events = []

    # Make sure the file exists!
    filename = os.path.abspath(filename)    
    if not os.path.exists(filename):
        return events

    with open(filename, 'rb') as file:

        current_event = None

        # use os.stat() to check the number of bytes.
        size = os.stat(filename).st_size

        # Loop through the file, reading 9 bytes at a time.
        for _ in range(0, size, 9):
            data = file.read(9)
            if len(data) != 9:
                raise result.TestFailure("Error: malformed event data file " + filename + "!")

            # Use a struct format string to read in the object.
            # https://docs.python.org/3/library/struct.html#struct.unpack
            # See the Python struct documentation: this says:
            # * use "standard" sizes in little (<) or big (>) endian mode.
            # * expect a 1-byte boolean (?)
            # * expect an "unsigned long long", which has a "standard" size of 8. (Q)
            format_string = "<?Q"
            if endian != 'little':
                format_string = ">" + format_string
            is_metadata, contents = struct.unpack(format_string, data)

            # Create a word object.
            word = DataWord(contents, is_metadata)

            # if this is a start-of-event word, let's look up the ID?
            # Do some more parsing here in order to accomplish this.
            if word.is_start_of_event():
                # This should be the lower 40 bits. Again, can retrieve with binary arithmetic.
                l0id = word.contents & (2**constants.l0id_size - 1)
                current_event = DataEvent(l0id)

            # Add the word to the current event we're reading.
            if current_event is not None:
                current_event.add_word(word)

            # Add the word to our event object
            if word.is_end_of_event():
                events.append(current_event)

    return events

def get_random_events(n_random, max_words=10, n_words=-1):
    """ Generates random event objects for testing."""
    events = []

    # TODO: _improve_ this function. Events generated here are not really
    # generated in a sensible way. I just used random.randint() to get going.

    # Look up lengths, compute how much is left.
    remaining_size = constants.word_length - constants.l0id_size - constants.flag_size

    for i in range(n_random):

        # Generate initial word.
        flag = constants.EVT_HDR_FLAG
        l0id = random.randint(0, 2**constants.l0id_size - 1)
        remaining = random.randint(0, 2**remaining_size - 1)

        event = DataEvent(l0id)

        # This assumes (as does the entire function, really)
        # that a header word is always: [FLAG][STUFF][FOOTER]
        contents = (2**(constants.word_length - constants.flag_size) * flag) + (2**(constants.l0id_size) * remaining) + l0id
        event.add_word(DataWord(contents, True))

        # Generate n random data words.
        n_words_int = random.randint(0, max_words)
        if n_words != -1:
            n_words_int = n_words
        for j in range(n_words_int):
            contents = random.randint(0, 2**constants.word_length - 1)
            event.add_word(DataWord(contents, False))

        # Generate footer word.
        flag = constants.EVT_FTR_FLAG
        remaining = random.randint(0, 2**(constants.word_length - constants.flag_size) - 1)
        contents = 2**(constants.word_length - constants.flag_size) * flag + remaining
        event.add_word(DataWord(contents, True))

        # Store generated event.
        events.append(event)

    return events

def initDataWord(contents, is_metadata=False):
    return DataWord(contents,is_metadata)

def words_in_event_list( event_list): 
    words_n = 0
    for event in event_list:
        print ("NEW EVENT\n")
        for word in event:
            print("\n\tWORD = ", word)
            words_n = words_n + 1
