// Spy Buffer "controller" module - using sentinel words.
// The spy buffer controller handles making the decision to
// record incoming information in the spy buffer.
// Ben Rosser <bjr@sas.upenn.edu>

// NOTE: this version of the block (TODO rename) uses sentinel bits
// in the event list to indicate wrap-around. This requires a state
// machine.

// NOTE: an implicit assumption here is made that we will NEVER
// have two start-of-event metadata words in a row.

// NOTE: the "readout" pieces here are probably going to go away,
// in the long term, and be replaced entirely by block RAM instantiations.
// It should be possible to have different readout controls altogether.
// e.g. some kind of block transfer protocol rather than just this.

`default_nettype none

module SpyController #(
		       // Width of data being passed into the system, *without* extra metadata bit.
		       parameter DATAWIDTH_A = 65,
		       parameter DATAWIDTH_B = 65,
		       parameter META_BIT_A = 64,
		       // Width of the spy buffer's spy memory. This is also the width of addresess.
		       parameter MEMWIDTH_A = 6,
		       parameter MEMWIDTH_B = 6,
		       
		       // Size *and* width of the event list FIFO memory.
		       parameter METASIZE = 16,
		       parameter METAWIDTH_A = 4,
		       parameter METAWIDTH_B = 4,

		       parameter SPY_META_DATA_WIDTH=6
		       ) (
			  /***********Ports in clock domain "clock"**************/
			  // Input wires -- clock, resetbar.
			  input wire 				clock,
			  input wire 				resetbar,
			  input wire 				playback_read,
			  input wire 				playback_write,
  // Inputs -- spy write enable and spy data.
			  input wire [DATAWIDTH_A-1:0] 		data_in,
			  input wire 				write_enable_in,
			  input wire [MEMWIDTH_A-1:0] 		addr,
			  output wire [DATAWIDTH_A-1:0] 	data_out, 
			  output reg [MEMWIDTH_A-1:0] 		mem_wptr, //Next address to be written to in normal mode			

   /*********** Ports in clock domain "spy_clock" *****************/
			  input wire 				spy_clock,
			  input wire 				spy_clock_meta,
			  input wire 				freeze, 
   // Inputs -- spy read enable and red address.
			  input wire 				spy_en,
			  input wire [MEMWIDTH_B-1:0] 		spy_addr,
			  input wire 				spy_wen,
			  input wire [DATAWIDTH_B-1:0] 		spy_data_in,
    // Same for the event / metadata list.
			  input wire 				spy_meta_en,
			  input wire [METAWIDTH_B-1:0] 		spy_meta_addr,
			  input wire [SPY_META_DATA_WIDTH-1:0] 	spy_meta_data_in,
			  input wire 				spy_meta_wen,
    // Output -- data being read out from spy memory.
			  output wire [DATAWIDTH_B-1:0] 	spy_data_out, 
    // Output -- metadata write address and occupancy.
    // These are the two things we could possibly need to read
			  // the metadata event list, regardless of which mode is used.
			  output reg [MEMWIDTH_B-1:0] 		flow_meta_write_addr,
			  output wire [SPY_META_DATA_WIDTH-1:0] flow_meta_data_out,
			  output wire [SPY_META_DATA_WIDTH-1:0] spy_meta_data_out,
			  output reg [MEMWIDTH_B-1:0] 		playback_mem_wptr,

			  input wire fifo_full
			  
);

    // Include the SpyProtocol.vh file.
    `include "SpyProtocol.vh"
   localparam MEM_BYTES_A = ((DATAWIDTH_A)>>3) + (DATAWIDTH_A & 7 != 3'b0 && (DATAWIDTH_A > 7));
   localparam MEM_BYTES_B = ((DATAWIDTH_B)>>3) + (DATAWIDTH_B & 7 != 3'b0 && (DATAWIDTH_B > 7));


   //parameter META_BYTES_A = ((METAWIDTH_A)>>3) + (METAWIDTH_A & 7 != 3'b0 && (METAWIDTH_A > 7));
   //parameter META_BYTES_B = ((METAWIDTH_B)>>3) + (METAWIDTH_B & 7 != 3'b0 && (METAWIDTH_B > 7));
   localparam META_BYTES_A = (METAWIDTH_A <=8 )? 1 : ((METAWIDTH_A)>>3) + (METAWIDTH_A & 7 != 3'b0 && (METAWIDTH_A > 7));
   localparam META_BYTES_B = (METAWIDTH_B <= 8 )? 1 : ((METAWIDTH_B)>>3) + (METAWIDTH_B & 7 != 3'b0 && (METAWIDTH_B > 7));

    // Define the states of the state machine
    // that controls the metadata FIFO.
    localparam STATE_RESET = 3'b000;
    localparam STATE_IDLE = 3'b001;
    localparam STATE_WRITE = 3'b010;
    localparam STATE_DATA_LOOP = 3'b011;
    localparam STATE_FROZEN = 3'b100;
    localparam STATE_EXTRA_LOOP = 3'b101;

    // Define the state registers.
   reg [2:0] 							state_reg;
   reg [2:0] 							state_next;
   
   // Internal version of spy write and read enables.
   reg 								write_enable;
   
   // We need to decide if this is the start of an event or not.
   // For now, let's assume the following naive protocol:
   // Data passed here will be 2**WIDTH + 1 bytes long.
   // The extra byte, if "1" will mean "SOE". Otherwise it's not.
   reg [DATAWIDTH_A-1:0] 					data;
   wire 							start_event;
   
   // Wire indicating that the memory has looped.
   wire 							mem_looped;
   
   
   // Control signals for the metadata list.
   reg 								ml_push;
   reg [SPY_META_DATA_WIDTH-1:0] 				ml_data;
   
   reg 								start_event_reg = 0;
   reg 								first_event;
   
   wire [MEMWIDTH_A-1:0] 					a_addr;
   wire [MEMWIDTH_B-1:0] 					b_addr;
   
   reg [1:0] 							freeze_d;
   

   reg 								n_fifo_full_d;
   
   
   assign mem_looped = (~| mem_wptr) & (~first_event);
//   assign  playback_mem_wptr = (playback_write & spy_wen)? spy_addr  : playback_mem_wptr;
   

   always @(posedge spy_clock, negedge resetbar) begin
      if(!resetbar)
	playback_mem_wptr <= 'b0;
      else
	begin
	   if(playback_write & spy_wen)
	     playback_mem_wptr <= spy_addr;	   
	end
   end
   
   //Move freeze signal to "clock" clock domain
    always @(posedge clock, negedge resetbar) begin
        if (!resetbar) begin
	   freeze_d      <= 2'b0;	
   	   n_fifo_full_d <= 0;
	   
	end
	else
	  begin
	     freeze_d <= {freeze_d[0], freeze};
	    // if(playback_read == 1 || playback_write == 1)
	    //   n_fifo_full_d <= ~fifo_full;
	  //   else
	       n_fifo_full_d <= 1;	     
	     
	  end
    end
   
   
    // At the rising edge of the clock-- propagate state.
    always @(posedge clock, negedge resetbar) begin
        if (!resetbar) begin
            state_reg <= STATE_RESET;
            start_event_reg <= 0;
            write_enable    <= 0;
	    mem_wptr        <= 0;
	    data            <= 0;
	   first_event      <= 1;
	   flow_meta_write_addr  <= 0;
	   
        end else begin
            state_reg <= state_next;
            start_event_reg <= start_event;
            write_enable    <= write_enable_in && (state_reg != STATE_FROZEN);
	    data            <= data_in;
	   if(write_enable)
	     mem_wptr       <= mem_wptr+1;
	   if(ml_push)
	     flow_meta_write_addr <= flow_meta_write_addr + 1;
	   

	   if(start_event_reg)
	     first_event <= 0;
	   	   
        end
    end

    // The state machine for the metadata FIF O(event list).
    always @* begin
        case (state_reg)
            STATE_RESET: begin
                ml_push = 0;
                ml_data = 0;
                state_next = (freeze_d[1] == 1)? STATE_FROZEN : STATE_WRITE;
            end

            // This is the write state; we know we're not frozen.
            // It's somewhat misleadingly named; this is essentially a "normal operations"
            // state, which could include that the system is idle.
            STATE_WRITE: begin
                ml_push = 0;
                ml_data = 0;
                state_next = STATE_WRITE;

                // If we're frozen, proceed immediately to STATE_FROZEN.
                if (freeze_d[1]) begin
                    state_next = STATE_FROZEN;
                end else if (write_enable) begin

                    // Now, we need to decide how to handle the metadata FIFO.
                    // If this is the start of event, we want to write the SOE word to the event list.
                    // If this write *will* loop the metadata FIFO, we want to-- on the next clock--
                    // insert a sentinel word into the FIFO.
                    // This handles the edge case. The only problem is that, in STATE_DATA_LOOP...
                    // we need to be able to duplicate all this logic without the loop check.

                    // In this condition, we must go to DATA_LOOP.
                    if (mem_looped && start_event_reg) begin
                        state_next = STATE_DATA_LOOP;

                        // Assign the ml data = current mem_wptr + no sentinel bit.
                       ml_data = {1'b0, mem_wptr}; //priya _reg};
                        ml_push = 1;

                    end else begin

                        // If mem_looped *xor* start event are set, write the correct thing.
                        // I guess it doesn't actually matter what is written in the sentinel word.
                        // It should matter in theory. some kind of control code.
                        if (mem_looped) begin
                           ml_data = {1'b1, mem_wptr};
                           ml_push = 1;
                        end else if (start_event_reg) begin
                           ml_data = {1'b0, mem_wptr}; 
                            ml_push = 1;
                        end else begin
                            ml_push = 0;
                        end

                        // Technically, this line is redundant.
                        state_next = STATE_WRITE;
                    end
                end
            end

            // This is our "we are about to loop in the memory FIFO" state.
            // This state does not interfere with normal writes.
            STATE_DATA_LOOP: begin
                // We need to handle the ml_data and ml_push writes.
               ml_data = {1'b1, mem_wptr}; //priya_reg};
                ml_push = 1;

                // Great, now we get to decide the next state!
                if (freeze_d[1]) begin
                    state_next = STATE_FROZEN;
                end else begin
                    state_next = STATE_WRITE;
                end
            end

            STATE_FROZEN: begin
                // If "freeze" is still held down, remain in state frozen and prevent writing.
                // TODO XXX: should we be able to go directly to STATE_WRITE from here? Probably not.

                ml_push = 0;
                ml_data = 0;

                if (freeze_d[1]) begin
                    state_next = STATE_FROZEN;
                end else begin
                    state_next = STATE_WRITE;
                end
            end

            // We need more states, I think.
            default: begin
                state_next = STATE_RESET;
                ml_push = 0;
                ml_data = 0;
            end
        endcase
    end

   // Refer to UG901 https://www.xilinx.com/support/documentation/sw_manuals/xilinx2019_2/ug901-vivado-synthesis.pdf
   // for the implementation of this module
   // Download the verilog implementation and put it under src/    
   // It turns out we probably can just use the *same* memory block for the event list.
    asym_ram_tdp_read_first #(
			     .WIDTHB(SPY_META_DATA_WIDTH), //MEMWIDTH_B + 1),
			     .SIZEB(2**METAWIDTH_B),
			     .ADDRWIDTHB(METAWIDTH_B),
			     .WIDTHA(SPY_META_DATA_WIDTH), //MEMWIDTH_A + 1),
			     .SIZEA(2**METAWIDTH_A),
			     .ADDRWIDTHA(METAWIDTH_A)
			     )  spy_metalist  (
					   .clkA(clock), 
					   .clkB(spy_clock_meta), 
					   .enaA(1'b1), 
					   .weA(ml_push), 
					   .enaB(spy_meta_en), 
					   .weB(spy_meta_wen), 
					   .addrA(flow_meta_write_addr), 
					   .addrB(spy_meta_addr), 
					   .diA(ml_data), 
					   .doA(flow_meta_data_out), //To DO - Need to use this for Playback
					   .diB(spy_meta_data_in),
					   .doB(spy_meta_data_out)
					   );
  
   assign a_addr = (playback_read)? addr : mem_wptr;
 
   
   // Instantiate the actual memory block used by the spy buffer.
     asym_ram_tdp_read_first #(
			     .WIDTHB(DATAWIDTH_B),
			     .SIZEB(2**MEMWIDTH_B),
			     .ADDRWIDTHB(MEMWIDTH_B),
			     .WIDTHA(DATAWIDTH_A),
			     .SIZEA(2**MEMWIDTH_A),
			     .ADDRWIDTHA(MEMWIDTH_A)
			     )  spy_memory  (
					   .clkA(clock), 
					   .clkB(spy_clock), 
					   .enaA(1'b1), 
					   .weA(write_enable), 
					   .enaB(spy_en), 
					   .weB(spy_wen), 
					   .addrA(a_addr), 
					   .addrB(spy_addr), 
					   .diA(data), 
					   .doA(data_out), 
					   .diB(spy_data_in),
					   .doB(spy_data_out)
					   );
   
  
    // I think this syntax is legal. Split "data_in".
    // We should do something smarter in the real system!
    // I think this (and the overflow detection code at the bottom) needs state-machine-ification.
    assign start_event = data_in[META_BIT_A] && (data_in[META_BIT_A-1:META_BIT_A-8] == START_EVENT);

    // TODO: This is redundant now: we store metadata bits!
    //assign data[DATAWIDTH:0] = data_in[DATAWIDTH:0];

endmodule
`default_nettype wire
