// SpyProtocol verilog include file.

// I chose these pretty arbitrarily. We can adjust as needed or add more
// valid metadata words.
localparam START_EVENT = 8'b10101011;
localparam END_EVENT = 8'b11001101;

// Playback states. (These could live in their own header).
localparam NO_PLAYBACK = 3'b00;
localparam START_PLAYBACK_ONCE = 3'b01;
localparam RUN_PLAYBACK_ONCE = 3'b101;
localparam PLAYBACK_LOOP = 3'b10;
localparam PLAYBACK_WRITE = 3'b11;

