# Constants, originally taken from the HTT/TP-specific DataFormat package,
# copied here to remove any dependency on that code.

# Changes to these parameters likely also require changes to the spy buffer
# constants in the RTL source.

# Flag that indicates this is the start-of-event word.
EVT_HDR_FLAG = 0xab

# Flag that indicates this is the end-of-event word.
EVT_FTR_FLAG = 0xcd

# Number of bits used for "L0ID" information-- this is how we distinguish
# events in this simple testbench. "Event ID" might be a more appropriate
# or generic name for this.
l0id_size = 40

# How many bits used for the flags-- see above for the two defined flags
# (but more could be implemented, in theory).
flag_size = 8

# The length of words.
word_length = 64
