# Example skeleton/dataflow tests for the TP,
# adapted to serve as spy buffer testbench.
# Ben Rosser <bjr@sas.upenn.edu>
import math
# Import cocotb.
import cocotb
from cocotb import triggers, result, clock

# Import our own packages.
from testbench import dataflow
from testbench import events, util
from testbench import spy

# Default clock speed. (ns)
clock_speed     = 4; #2.7 #200
spy_clock_speed = 10 #2000


def initialize_wires(dut):
    """ Initializes top-level signals to zero in the wrapper.
        By setting things to zero explicitly, stops high z states."""
    wires = [dut.we, dut.re, dut.data_in, dut.meta_read_ptr, 
              dut.spy_read_ptr, dut.playback_we, dut.playback_write_data,
             dut.playback, dut.freeze]
    dut.re <= 1
    for wire in wires:
        wire <= 0


def update_input_width(dut):
    """If Block RAM port width does not match input data flow width, then determine width of zero padding to be done"""
    return len(dut.spy_buffer.write_data)

def update_input_zp(dut):
    return len(dut.spy_buffer.write_data) - len(dut.data_in)

def get_meta_bit_pos(dut):
    return (update_input_width(dut) - update_input_zp(dut) - 1)

def get_spy_words_in_df(dut):
    return math.ceil(len(dut.spy_buffer.write_data)/len(dut.data_out))


@cocotb.coroutine
def reset(dut):
    """ Reset the blocks on start."""
    dut.reset <= 1
    yield triggers.ClockCycles(dut.clock, 10)
    dut.reset <= 0
    yield triggers.ClockCycles(dut.clock, 1)
    dut.reset <= 1

@cocotb.test()
def test_001_initial_dataflow(dut):
    """ Initial test of dataflow driver and monitor."""
    # Default: one 200ns clock for now. Could be changed!
    # This can be moved to a testbench class later.
    sim_clock     = clock.Clock(dut.clock, clock_speed, 'ns')
    spy_sim_clock = clock.Clock(dut.spy_clock, spy_clock_speed, 'ns')
    cocotb.fork(sim_clock.start())
    cocotb.fork(spy_sim_clock.start())

    # Reset everything?
    initialize_wires(dut)

    yield reset(dut)

    dut._log.info("Initialized and reset spy buffer testbench.")

    # Initialize a data flow object.
    flow = dataflow.DataflowController(dut, dut.clock, input_width=update_input_width(dut), input_zp=update_input_zp(dut))

    # Add a driver to the input; let's call it "Input".
    flow.add_input_fifo("Input", dut.spy_buffer)

    # Add a driver to the output; let's call it "Output".
    flow.add_output_fifo("Output", dut.spy_buffer)

    # Start the dataflow checker.
    # Right now, this only checks that start/end of event words
    # are in the right place.
    flow.start()

    # Let's generate some random fake events.
    # Yield on the return, which blocks until all the events are sent.
    # We could load a file here instead!
    hook, random_events = flow.send_random_events("Input", 10)
    yield hook.wait()

    # Now that we've finished sending data. Wait until all the monitors are finished.
    yield flow.check_outstanding(timeout=10000, units='ns')

    # We probably _do_ want to explicitly call the stop() function here
    # since it enables us to have a place to close output files.
    flow.stop()

@cocotb.test()
def test_002_freeze_unfreeze(dut):
    """ Test the freeze mechanism."""
    # Default: one 200ns clock for now. Could be changed!
    # This can be moved to a testbench class later.
    sim_clock     = clock.Clock(dut.clock, clock_speed, 'ns')
    spy_sim_clock = clock.Clock(dut.spy_clock, spy_clock_speed, 'ns')
    cocotb.fork(sim_clock.start())
    cocotb.fork(spy_sim_clock.start())




    # Reset everything.
    initialize_wires(dut)
    yield reset(dut)
    dut._log.info("Initialized and reset spy buffer testbench.")

    # Initialize a data flow object with drivers and start it.
    flow = dataflow.DataflowController(dut, dut.clock, input_width=update_input_width(dut), input_zp=update_input_zp(dut))
    flow.add_input_fifo("Input", dut.spy_buffer)
    flow.add_output_fifo("Output", dut.spy_buffer)
    flow.start()

    # Also initialize a spy interface!
    interface = spy.SpyInterface(dut, dut.spy_clock, dut.spy_buffer)

    # Freeze the spy buffer and send a random event.
    # It should succeed!
    yield interface.freeze()
    
    hook, random_events = flow.send_random_events("Input", 2)
    yield hook.wait()

    # Make sure that the spy buffer write pointers are zero.
    if dut.spy_buffer.dbg_spy_write_addr.value != 0:
        raise result.TestFailure("Error: something was written to spy memory while frozen")
    if dut.spy_buffer.dbg_spy_meta_write_addr.value != 0:
        raise result.TestFailure("Error: something was written to spy event list while frozen")

    # Unfreeze, make sure we can still send an event!
    yield interface.unfreeze()
   
    hook, random_events = flow.send_random_events("Input", 2)
    yield hook.wait()

    # Make sure something _was_ written to the spy memory.
    if dut.spy_buffer.dbg_spy_write_addr.value == 0:
        raise result.TestFailure("Error: something was not written to spy memory while not frozen")
    if dut.spy_buffer.dbg_spy_meta_write_addr.value == 0:
        raise result.TestFailure("Error: something was not written to spy event list while not frozen")

    # Cleanup and checking.
    yield flow.check_outstanding(timeout=10000, units='ns')
    flow.stop()

@cocotb.test()
def test_003_spy_readout_basic(dut):
    """ Test that the spy memories can be read back while frozen."""
    # Default: one 200ns clock for now. Could be changed!
    # This can be moved to a testbench class later.
    sim_clock     = clock.Clock(dut.clock, clock_speed, 'ns')
    spy_sim_clock = clock.Clock(dut.spy_clock, spy_clock_speed, 'ns')
    cocotb.fork(sim_clock.start())
    cocotb.fork(spy_sim_clock.start())

   

    # Reset everything.
    initialize_wires(dut)
    yield reset(dut)
    dut._log.info("Initialized and reset spy buffer testbench.")

    # Initialize a data flow object with drivers and start it.
    flow = dataflow.DataflowController(dut, dut.clock, input_width=update_input_width(dut), input_zp=update_input_zp(dut))    
    flow.add_input_fifo("Input", dut.spy_buffer)
    flow.add_output_fifo("Output", dut.spy_buffer)
    flow.start()

    # Also initialize a spy interface!
    interface = spy.SpyInterface(dut, dut.spy_clock, dut.spy_buffer, spy_dwidth=len(dut.data_out), spy_words_in_df = get_spy_words_in_df(dut), meta_bit = get_meta_bit_pos(dut) )

    # Send some random events, and make sure we get them.
    hook, random_events = flow.send_random_events("Input", 2)
    yield hook.wait()
    yield flow.check_outstanding(timeout=40000, units='ns')

    # Now... freeze the spy buffer. See if we can read out the second event.
    # Note: this deliberately just tries to read address 1 in the meta event
    # list rather than use the algorithm; the algorithm is tested in test 4.
    yield interface.freeze()
    

    # This might be a little fragile. I think we don't allow the random
    # events to be large enough for two to fill the spy buffer, though,
    # so the first entry _should_ always be the start of the second event.
    contents, _ = yield interface.read_event_list_address(1)
    event_start = int(contents)
    dut._log.info("Second event in spy memory starts at address " + str(event_start))

    current_wptr = int(dut.spy_buffer.dbg_spy_write_addr.value)
    dut._log.info("Spy buffer write pointer at address " + str(current_wptr))
    if (current_wptr - event_start) != len(random_events[1]):
        raise result.TestFailure("Error: full event not written to spy memory.")

    # Read from the start of the second event to the current spy memory write pointer.
    # This should be a complete event, if everything is working.
    spy_event = yield interface.read_event(event_start, current_wptr)
    dut._log.info("Read out event with L0ID " + hex(spy_event.l0id) + " from spy memory.")
    if spy_event.l0id != random_events[1].l0id:
        raise result.TestFailure("Error: expected to find event with L0ID " + hex(random_events[1].l0id))

    flow.stop()

@cocotb.test()
def test_004_spy_readout_algorithm(dut):
    """ Test that we can readout the spy memory using the event list algorithm."""
    # Default: one 200ns clock for now. Could be changed!
    # This can be moved to a testbench class later.
    sim_clock     = clock.Clock(dut.clock, clock_speed, 'ns')
    spy_sim_clock = clock.Clock(dut.spy_clock, spy_clock_speed, 'ns')
    cocotb.fork(sim_clock.start())
    cocotb.fork(spy_sim_clock.start())


    # Reset everything.
    initialize_wires(dut)
    yield reset(dut)
    dut._log.info("Initialized and reset spy buffer testbench.")

    # Initialize a data flow object with drivers and start it.
    flow = dataflow.DataflowController(dut, dut.clock, input_width=update_input_width(dut), input_zp=update_input_zp(dut))        
    flow.add_input_fifo("Input", dut.spy_buffer)
    flow.add_output_fifo("Output", dut.spy_buffer)
    flow.start()

    # Also initialize a spy interface!
    interface = spy.SpyInterface(dut, dut.spy_clock, dut.spy_buffer, spy_dwidth=len(dut.data_out), spy_words_in_df = get_spy_words_in_df(dut), meta_bit = get_meta_bit_pos(dut) )
  
    total_events = 3
    # Send 3 events of size 127. (125 + two metadata words).
    # This ensures that there will be multiple sentinel words in
    # the event list indicating wrap-around.
    for i in range(total_events):
        large_event = events.get_random_events(1, n_words=125)
        hook = flow.send_event(large_event[0], "Input")
        yield hook.wait()

    # Now, send 3 random events. Make sure we wait until we receive everything.
    hook, random_events = flow.send_random_events("Input", total_events)
    yield hook.wait()
    yield flow.check_outstanding(timeout=80000, units='ns')

    # Freeze for readout.
    yield interface.freeze()

    # Now... let's use the higher-level functions in the spy interface
    # to read out the spy memory.
    spy_events = yield interface.read_events()
    if len(spy_events) != 3:
        raise result.TestFailure("Error: expected to find 3 events, instead read " + str(len(spy_events)) + " from memory.")
    for index, spy_event in enumerate(spy_events):
        # This... should be better.
        expected = random_events[-index-1]
        if spy_event.l0id != expected.l0id:
            raise result.TestFailure("Error: event ID mismatch. Expected (" + str(expected) + ") vs observed (" + str(spy_event.l0id) + ")")

        if len(spy_event) != len(expected):
            raise result.TestFailure("Error: event size mismatch. Expected (" + str(len(expected)) + ") vs observed (" + str(len(spy_event)) + ")")

    flow.stop()

@cocotb.test()
def test_005_playback_states(dut):
    """ Test that events can't be sent while in a playback state."""
    # Default: one 200ns clock for now. Could be changed!
    # This can be moved to a testbench class later.
    sim_clock     = clock.Clock(dut.clock, clock_speed, 'ns')
    spy_sim_clock = clock.Clock(dut.spy_clock, spy_clock_speed, 'ns')
    cocotb.fork(sim_clock.start())
    cocotb.fork(spy_sim_clock.start())
 

    # Reset everything.
    initialize_wires(dut)
    yield reset(dut)
    dut._log.info("Initialized and reset spy buffer testbench.")

    # Initialize a data flow object with drivers and start it.
    flow = dataflow.DataflowController(dut, dut.clock, input_width=update_input_width(dut), input_zp=update_input_zp(dut))        
    flow.add_input_fifo("Input", dut.spy_buffer)
    flow.add_output_fifo("Output", dut.spy_buffer)
    flow.start()

    # Also initialize a spy interface!    
    interface = spy.SpyInterface(dut, dut.spy_clock, dut.spy_buffer, spy_dwidth=len(dut.data_out), spy_words_in_df = get_spy_words_in_df(dut), meta_bit = get_meta_bit_pos(dut) )

    # For each non NO_PLAYBACK state, ensure we fail to send an event.
    # Actually, we can only really use the PLAYBACK_WRITE state for this.
    # Unfortunately, the other states will immediately try to read whatever's
    # in the spy memory... and there's no concept of the spy memory being "empty",
    # and I'm not sure the monitor can easily be made to do the right thing.
    # This is okay as long as the internal logic is "playback == NO_PLAYBACK", which
    # it currently is.
    for state in [spy.PLAYBACK_WRITE]:
        interface.set_playback(state)
        yield triggers.RisingEdge(dut.clock)
        hook, random_events = flow.send_random_events("Input", 1, expect_fail=True)
        yield hook.wait()

    # Set the state back to NO_PLAYBACK and ensure we can send an event now.
    interface.set_playback(spy.NO_PLAYBACK)
    yield triggers.RisingEdge(dut.clock)
    hook, random_events = flow.send_random_events("Input", 1)
    yield hook.wait()

    # Cleanup and checking.
    yield flow.check_outstanding(timeout=10000, units='ns')
    flow.stop()

@cocotb.test()
def test_006_playback_once(dut):
    """ Attempt to playback two events through the spy buffer, once."""
    # Default: one 200ns clock for now. Could be changed!
    # This can be moved to a testbench class later.
    sim_clock     = clock.Clock(dut.clock, clock_speed, 'ns')
    spy_sim_clock = clock.Clock(dut.spy_clock, spy_clock_speed, 'ns')
    cocotb.fork(sim_clock.start())
    cocotb.fork(spy_sim_clock.start())


    # Reset everything.
    initialize_wires(dut)
    yield reset(dut)
    dut._log.info("Initialized and reset spy buffer testbench.")

    # Initialize a data flow object with drivers and start it.
    flow = dataflow.DataflowController(dut, dut.clock, input_width=update_input_width(dut), input_zp=update_input_zp(dut))        
    flow.add_input_fifo("Input", dut.spy_buffer)
    flow.add_output_fifo("Output", dut.spy_buffer)
    flow.start()

    # Also initialize a spy interface!    
    interface = spy.SpyInterface(dut, dut.spy_clock, dut.spy_buffer, spy_dwidth=len(dut.data_out), spy_words_in_df = get_spy_words_in_df(dut), meta_bit = get_meta_bit_pos(dut) )

    # Freeze the spy buffer 
    yield interface.freeze()
    
    # Set the spy buffer to PLAYBACK_WRITE.
    interface.set_playback(spy.PLAYBACK_WRITE)
    yield reset(dut)

    # Generate two random events and load them in via playback.
    playback_events = events.get_random_events(2)
    n_words = yield interface.write_events(playback_events)
    if n_words == 0:
        raise result.TestFailure("Error: could not write any words in PLAYBACK_WRITE state.")

    # Tell the monitor to expect these events. NOTE: this currently will just check IDs.
    for event in playback_events:
        flow.output_fifos["Output"].expected_ids.add(event.l0id)
  

    # Now... set the state to playback once!
    # And then use check_outstanding() to wait until we saw all L0IDs.
    interface.set_playback(spy.PLAYBACK_ONCE)
    yield flow.check_outstanding(timeout=40000, units='ns')
    

    flow.stop()
    dut._log.info("Halting playback.")
    interface.set_playback(spy.NO_PLAYBACK)
    yield reset(dut)


@cocotb.test()
def test_007_playback_loop(dut):
    """ Attempt to playback events through the spy buffer in loop mode."""
    # Default: one 200ns clock for now. Could be changed!
    # This can be moved to a testbench class later.
    sim_clock = clock.Clock(dut.clock, clock_speed, 'ns')
    spy_sim_clock = clock.Clock(dut.spy_clock, spy_clock_speed, 'ns')
    cocotb.fork(sim_clock.start())
    cocotb.fork(spy_sim_clock.start())


    # Reset everything.
    initialize_wires(dut)
    yield reset(dut)
    dut._log.info("Initialized and reset spy buffer testbench.")

    # Initialize a data flow object with drivers and start it.
    flow = dataflow.DataflowController(dut, dut.clock, input_width=update_input_width(dut), input_zp=update_input_zp(dut))        
    flow.add_input_fifo("Input", dut.spy_buffer)
    flow.add_output_fifo("Output", dut.spy_buffer)
    flow.start()

    # Also initialize a spy interface!
    interface = spy.SpyInterface(dut, dut.spy_clock, dut.spy_buffer, spy_dwidth=len(dut.data_out), spy_words_in_df = get_spy_words_in_df(dut), meta_bit = get_meta_bit_pos(dut) )

    # Set the spy buffer to PLAYBACK_WRITE.
    interface.set_playback(spy.PLAYBACK_WRITE)
    yield reset(dut)

    # Generate two random events and load them in via playback.
    playback_events = events.get_random_events(2)
    n_words = yield interface.write_events(playback_events)
    if n_words == 0:
        raise result.TestFailure("Error: could not write any words in PLAYBACK_WRITE state.")

    # Tell the monitor to expect these events. NOTE: this currently will just check IDs.
    for event in playback_events:
        flow.output_fifos["Output"].expected_ids.add(event.l0id)


    # Set the playback state to PLAYBACK_LOOP. This is a bit tricky, but fortunately
    # basic playback functionality is already checked above.
    interface.set_playback(spy.PLAYBACK_LOOP)
    dut._log.info("Waiting to see events a first time...")
    yield flow.check_outstanding(timeout=10000, units='ns')

    # Now... if all has gone well, we should see the same events again.
    for event in playback_events:
        flow.output_fifos["Output"].expected_ids.add(event.l0id)
    dut._log.info("Waiting to see events a second time...")
    yield flow.check_outstanding(timeout=10000, units='ns')

    # If we did (and if we didn't see anything unexpected, which should be flagged
    # automatically), then pass the test and stop playback.
    flow.stop()
    dut._log.info("Halting playback.")
    interface.set_playback(spy.NO_PLAYBACK)
    yield reset(dut)




#@cocotb.test()
def test_008_custom_playback_once(dut):
    """ Attempt to playback  events through the spy buffer, once."""
    # Default: one 200ns clock for now. Could be changed!
    # This can be moved to a testbench class later.
    sim_clock     = clock.Clock(dut.clock, clock_speed, 'ns')
    spy_sim_clock = clock.Clock(dut.spy_clock, spy_clock_speed, 'ns')
    cocotb.fork(sim_clock.start())
    cocotb.fork(spy_sim_clock.start())


    # Reset everything.
    initialize_wires(dut)
    yield reset(dut)
    dut.re = 1
    dut._log.info("Initialized and reset spy buffer testbench.")

    # Initialize a data flow object with drivers and start it.
    flow = dataflow.DataflowController(dut, dut.clock, input_width=update_input_width(dut), input_zp=update_input_zp(dut))        
    flow.add_input_fifo("Input", dut.spy_buffer)
    flow.add_output_fifo("Output", dut.spy_buffer)
    flow.start()

    # Also initialize a spy interface!    
    interface = spy.SpyInterface(dut, dut.spy_clock, dut.spy_buffer, spy_dwidth=len(dut.data_out), spy_words_in_df = get_spy_words_in_df(dut), meta_bit = get_meta_bit_pos(dut) )

    # Freeze the spy buffer 
    yield interface.freeze()

    # Set the spy buffer to PLAYBACK_WRITE.
    interface.set_playback(spy.PLAYBACK_WRITE)


    # Generate two random events with more words and load them in via playback.
    playback_events_list = []
    playback_events = events.get_random_events(1, n_words = 125)
    for event in playback_events:
        print ("Total words in playback events = ", len(event.words))
        print ("\n\t WORDS in event = ", util.hex_list(event.words))
    
    n_words = yield interface.write_events(playback_events)
    if n_words == 0:
        raise result.TestFailure("Error: could not write any words in PLAYBACK_WRITE state.")

    # Tell the monitor to expect these events. NOTE: this currently will just check IDs.
    for event in playback_events:
        flow.output_fifos["Output"].expected_ids.add(event.l0id)

    # Now... set the state to playback once!
    # And then use check_outstanding() to wait until we saw all L0IDs.
    interface.set_playback(spy.PLAYBACK_ONCE)

    #interface.set_playback(spy.NO_PLAYBACK)
    #yield triggers.ClockCycles(dut.clock, 10)
    #interface.set_playback(spy.PLAYBACK_ONCE)
    #yield triggers.ClockCycles(dut.clock, 10000)
    yield flow.check_outstanding(timeout=10000, units='ns')

    dut.re = 0
    yield triggers.ClockCycles(dut.clock, 100)
    interface.set_playback(spy.NO_PLAYBACK)
    yield triggers.ClockCycles(dut.clock, 100)

    #Run Second time
   # Tell the monitor to expect these events. NOTE: this currently will just check IDs.
    for event in playback_events:
        flow.output_fifos["Output"].expected_ids.add(event.l0id)

   
    interface.set_playback(spy.PLAYBACK_ONCE)
    yield triggers.ClockCycles(dut.clock, 4)
    dut.re = 1
    yield triggers.ClockCycles(dut.clock, 40000)
    #yield flow.check_outstanding(timeout=10000, units='ns')
    flow.stop()
    yield reset(dut)

