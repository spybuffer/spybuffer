create_clock -period 3.125 -name flow_clk -waveform {0.000 1.563} [get_ports {rclock wclock}]
create_clock -period 10.000 -name spy_clock -waveform {0.000 5.000} [get_ports {spy_clock spy_clock_meta}]
