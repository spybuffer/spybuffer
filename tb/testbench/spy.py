import cocotb
from cocotb import triggers, result

from . import events, util

NO_PLAYBACK = 0
PLAYBACK_ONCE = 1
PLAYBACK_LOOP = 2
PLAYBACK_WRITE = 3

class SpyInterface(object):

    def __init__(self, dut, spy_clock, path, spy_dwidth=64, spy_words_in_df=1, meta_bit=64):
        self.dut = dut

        # This spy_clock should be the DUT's  "spy_clock".
        self.spy_clock = spy_clock

        # Path to the spy buffer that this interface is wrapping.
        self.spy = path

        # Store this in Python, just in case it's useful.
        # Other code may eventually want to quickly (Pythonically)
        # query this.
        self.frozen = False

        # Similarly, track the playback state in Python just in case it's useful.
        self.playback = NO_PLAYBACK
        
        self.spy_dwidth      = spy_dwidth
        self.spy_words_in_df = spy_words_in_df
        self.meta_bit        = meta_bit

    @cocotb.coroutine
    def freeze(self):
        """ Freeze the spy buffer."""
        self.spy.freeze <= 1
        self.frozen = True
        yield triggers.RisingEdge(self.spy_clock) 
        yield triggers.ClockCycles(self.spy_clock, 2) #Extra clock to move clock domains

    @cocotb.coroutine
    def unfreeze(self):
        """ Un-freeze the spy buffer."""
        self.spy.freeze <= 0
        self.frozen = False
        yield triggers.RisingEdge(self.spy_clock)
        yield triggers.ClockCycles(self.spy_clock, 2) #Extra clock to move clock domains

    def set_playback(self, state):
        if state < 0 or state > 3:
            self.dut._log.info("Error: tried to set illegal playback state!")
        else:
            self.playback = state
            self.spy.playback = state

    @cocotb.coroutine
    def read_event_list_address(self, address):
        """ Reads the contents of address 'address' from the spy event list.
            Returns a tuple of [contents, sentinel flag]."""
        if not self.frozen or self.playback != NO_PLAYBACK:
            raise result.ReturnValue(None)

        # Set the read address to "address", toggle the read enable.
        self.spy.spy_meta_addr <= address       
        yield triggers.RisingEdge(self.spy_clock)
        yield triggers.ReadOnly()
        value = self.spy.spy_meta_read_data.value
        yield triggers.NextTimeStep()
        

        # Convert "value" to a little endian object.
        value = util.BinaryValue(int(value), n_bits=len(value))
        
        # Process the output. I hope this works?
        is_sentinel = bool(value[len(value)-1])
        contents = int(value[len(value)-2:0])
        raise result.ReturnValue((contents, is_sentinel))

    @cocotb.coroutine
    def read_address(self, address):
        """ Reads the contents of address 'address' from the spy memory."""
        if not self.frozen or self.playback != NO_PLAYBACK:
            raise result.ReturnValue(None)

        self.spy.spy_write_enable <= 0

        # Set the read address to "address", toggle the read enable.
        spy_value   = 0
        spy_addr    = 0
        max_address = 2**len(self.spy.dbg_spy_meta_write_addr) * self.spy_words_in_df
        spy_addr    = (address * self.spy_words_in_df) % max_address
        for i in range(self.spy_words_in_df):  
            self.spy.spy_addr          <= spy_addr + i
            yield triggers.RisingEdge(self.spy_clock)
            yield triggers.ReadOnly()
            value     = self.spy.spy_data.value
            #print("DEBUG INTERFACE SPY_DATA = ", util.hex(value))
            spy_value = spy_value + (value << i*self.spy_dwidth)
            yield triggers.NextTimeStep()
            
            
        # Convert "value" to a little endian object.
        spy_value = util.BinaryValue(int(spy_value), n_bits=len(value)*self.spy_words_in_df)

        # Process the output. I hope this works?
        is_metadata = bool(spy_value[self.meta_bit])
        contents = int(spy_value[self.meta_bit-1:0])
        raise result.ReturnValue((contents, is_metadata))

    @cocotb.coroutine
    def read_event_list(self):
        """ Read out the event list / metadata list."""
        # If the block isn't frozen, no-op. NOTE: this is an implementation
        # choice, we could easily just do a freeze instead.
        event_list = []
        if not self.frozen or self.playback != NO_PLAYBACK:
            raise result.ReturnValue(event_list)

        # Get the current value of the _write_ pointer in the spy event list.
        # Also find out the size of the address-- I think calling length will work?
        max_address = 2**len(self.spy.dbg_spy_meta_write_addr) 
        address = int(self.spy.dbg_spy_meta_write_addr.value) - 1

        # Get the current value of the write pointer in the spy buffer's main memory.
        oldest = int(self.spy.dbg_spy_write_addr.value)
        event_list.append(oldest)

        # Read the address!
        seen_sentinels = 0
        for i in range(max_address):
            real_addr = (address - i) % max_address
            contents, is_sentinel = yield self.read_event_list_address(real_addr)
            seen_sentinels += int(is_sentinel)

            # If we see *two* sentinel words, we stop.
            if seen_sentinels == 2:
                break

            # If we have already seen *one* sentinel, and the address read back
            # is less than the contents of the current write pointer in the
            # main memory, then we're looking at events we have already overwritten,
            # so drop them.
            if seen_sentinels == 1 and contents <= oldest:
                continue

            # Otherwise, add the read-back value to the event list.
            if not is_sentinel:
                event_list.append(contents)

        raise result.ReturnValue(event_list)

    @cocotb.coroutine
    def read_event(self, start, end=-1):
        """ Reads an 'event' in the spy memory from start address to end address.
            Returns event object containing list of all words read."""
        event         = None
        end_undefined = False
        if not self.frozen or self.playback != NO_PLAYBACK:
            raise result.ReturnValue(event)

        # Memory length can be inferred somewhat awkwardly.
        max_address = 2**len(self.spy.dbg_spy_write_addr)
        if end == -1:
            end           = max_address
            end_undefined = True

        # Handle edge case where we need range() to be circular.
        if start > end :
            end += max_address

        
        for address in range(start, end):
            actual = address % max_address
           
            spy_data = 0
                  
                
            contents, is_metadata = yield self.read_address(actual)
            word = events.DataWord(contents, is_metadata)

            # Handle the case where the event boundary is... not actually an event boundary.
            if address == start and not word.is_start_of_event():
                raise result.TestFailure("Error: event at address " + str(start) + " did not begin with start-of-event.")

            # Handle the case that this is the start of an event!
            if word.is_start_of_event():
                if not event is None:
                    # Something's gone wrong-- we've seen multiple starts of event.
                    self.dut._log.info("Saw second start-of-event before end-of-event at address " + str(actual))
                    raise result.TestFailure("Error: problem parsing event with L0ID " + util.hex(event.l0id))
                l0id = events.get_l0id_from_event(word)
                event = events.DataEvent(l0id)

            # Add the word to the event.
            event.add_word(word)

            # If this is an _end_ of event and address != end - 1, then complain.
            if word.is_end_of_event() and address != end - 1:
                if end_undefined:
                    break
                else:
                    self.dut._log.info("Saw end-of-event before expectation from event boundaries at address " + str(actual))
                    raise result.TestFailure("Error: problem parsing event with L0ID " + util.hex(event.l0id) + "Current address = " +util.hex(address) + " End Address = " + util.hex(end))

        # Return the event.
        raise result.ReturnValue(event)

    @cocotb.coroutine
    def read_events(self):
        """ Reads all events from the spy memory using the event list.
            Returns list of event objects sorted from newest to oldest."""
        read_events = []
        if not self.frozen or self.playback != NO_PLAYBACK:
            raise result.ReturnValue(read_events)

        # Get the event list. This contains: the current write pointer in the spy memory
        # as the first entry, followed by the addresses of start-of-event words.
        # So, to read an event, we can read from the *second* element in the parsed event
        # list to the *first* (the current write pointer)-- this will probably be an incomplete
        # event. Then we can read from the *third* to the *second* to get a complete event, and
        # so on until we get to the last start-of-event word.
        event_list = yield self.read_event_list()
        self.dut._log.info("Read event list: " + str(event_list))

        # This is a case where the event list is, in fact, empty.
        # That means there are no start-of-event words to read, so... return [].
        if len(event_list) < 2:
            raise result.ReturnValue(read_events)

        # Now, we know that there will be at least two entries:
        # i = end of event (or current write pointer) and i+1 =(start of event).
        for i, address in enumerate(event_list):
            if i + 1 == len(event_list):
                break
            start_event_address = event_list[i+1]
            event = yield self.read_event(start_event_address, address)
            if event is None:
                raise result.TestFailure("Failed to read event from address " + str(start_event_address) + " to " + str(address))
            self.dut._log.info("Read event with L0ID " + hex(event.l0id) + " and " + str(len(event.words)) + " words from spy memory.")
            read_events.append(event)

        raise result.ReturnValue(read_events)


    @cocotb.coroutine
    def write_address(self, address, data):
        
        if self.playback != PLAYBACK_WRITE:
            self.dut._log.info("Cannot write events if not in playback write state.")
            raise result.ReturnValue(0)
    
        # Set the read address to "address", toggle the read enable.
        spy_addr    = 0
        max_address = 2**len(self.spy.dbg_spy_meta_write_addr) * self.spy_words_in_df
        spy_addr    = (address * self.spy_words_in_df) % max_address
        for i in range(self.spy_words_in_df):  
            self.spy.spy_addr          <= spy_addr + i
            self.spy.spy_write_data    <= ( (data.get_binary() >> (i*self.spy_dwidth)) ) & ( (1 << self.spy_dwidth) -1 ) 
            self.spy.spy_write_enable  <= 1
            yield triggers.RisingEdge(self.spy_clock)
        self.spy.spy_write_enable <= 0
        yield triggers.NextTimeStep()

        # Probably not necessary.
        raise result.ReturnValue(address)


    @cocotb.coroutine
    def write_events(self, events_to_write):
        """ Writes events into the spy buffer memory for playback.
            Block must be in the PLAYBACK_WRITE state.
            Returns the number of words written."""
        if self.playback != PLAYBACK_WRITE:
            self.dut._log.info("Cannot write events if not in playback write state.")
            raise result.ReturnValue(0)

        # THiS DOES NOT set the current write pointer.
        # So the block needs to be reset before calling this method.
        count = 0

        if self.spy.almost_full != 1:       
           # yield triggers.RisingEdge(self.spy_clock)
            for event in events_to_write:
                self.dut._log.info("Writing event with L0ID " + hex(event.l0id) + " and " + str(len(event.words)) + " words to spy memory.")
            
                for word in event:
                    # For each word in each event; write it!
                    # Hold the playback write enable high until we're done.
                    #self.spy.spy_write_data   <= word.get_binary()
                    #self.spy.spy_write_enable <= 1
                    #self.spy.spy_addr              <= count
                    #yield triggers.RisingEdge(self.spy_clock)
                    if self.spy.almost_full != 1:       
                        yield self.write_address(count, word)
                        count += 1
                        self.spy.spy_write_enable <= 0
                
                # There's a spy_clock of latency before this updates.
                yield triggers.ClockCycles(self.spy_clock, 2)
                self.dut._log.info("Current spy write pointer = " + str(int(self.spy.dbg_spy_write_addr.value)))

            if self.spy.almost_full != 1:       
                self.spy.spy_write_enable = 0
        # Probably not necessary.
        raise result.ReturnValue(count)
