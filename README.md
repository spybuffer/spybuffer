# Spy Buffer

This repository contains a Verilog implementation of a "spy buffer"
firmware block. The spy buffer firmware block is intended to be instantiated
between firmware blocks in a FPGA design and provide both flow control--
through an included FIFO-- and monitoring, through a "spy" memory which
stores each word that passes from block to block into a memory buffer,
which can later be read out for analysis and debugging.

The implementation is pure Verilog; it does not use any vendor-specific
IP blocks. It is intended that the design should be usable for both AMD
and Intel FPGA designs.

More information on how the spy buffer works is described in the "Spy Buffer
Interface Documentation" section (below the notes on verification).

The asynchronous FIFO implementation included in this repository is,
at the moment, ultimately derived from [a paper](http://www.sunburst-design.com/papers/CummingsSNUG2002SJ_FIFO2.pdf)
by Clifford E. Cummings of Sun Burst Design, Inc. presented at the
Synopsys Users Group (SNUG) meeting in 2002. 

The rest of the code was originally developed for use on the ATLAS
Hardware Tracking for the Trigger upgrade project at the University of
Pennsylvania, and is made freely available under the MIT license.

# Spy Buffer Verification

This repository contains a series of unit tests for the spy buffer,
located in [tb/](tb/). These tests are designed to verify basic spy
buffer features, including:

* Basic data flow.
* Reading of events from the spy memory.
* Playback of events written into the spy memory.
* Operations both in passthrough mode (without the FIFO) and in normal
mode (with the FIFO).


## Requirements and Usage

These tests use [cocotb](https://github.com/cocotb/cocotb) and are written
in Python. Cocotb is the only external dependency required to run the tests;
you will also need a RTL simulator [supported by cocotb](https://docs.cocotb.org/en/stable/simulator_support.html)
such as Questa or Cadence.

The SpyBuffer memory implementation is not included in this repository, 
but `asym_ram_tdp_read_first` can be used, written in Verilog, as described in [this document]( https://www.xilinx.com/support/documentation/sw_manuals/xilinx2019_2/ug901-vivado-synthesis.pdf). The aforementioned Verilog file must be added as a file named `asym_ram_tdp_read_first.v` under `src`.

A simple way to install cocotb and get started would be to set up a
[virtual environment](https://docs.python.org/3/tutorial/venv.html) and
run pip (but the package could also be installed systemwide or through
other means). To set up a virtual environment, you can run the following
commands on a Unix system (it should be possible to do something similar
on a Windows system as well):

```
cd tb/
python3 -m venv env
source env/bin/activate
pip install cocotb
pip install cocotb_bus
```

This will create a virtual environment in the `tb/env` directory, activate
it, and install the cocotb package. Once created, you can activate it at
any time by running `source env/bin/activate`, and deactivate it by simply
running `deactivate`.
However you install cocotb, you can confirm that cocotb is present and working
by running `cocotb-config --prefix`; this should point at your cocotb installation
(which would be in `tb/env` if you use a virtual environment).

Recent releases of cocotb require Python 3; the tests at the moment should
be backwards compatible with cocotb 1.3 and Python 2.7, but that compatibility
is not guaranteed to remain.

The simulator is specified by setting the `SIM` variable; by default, this
is set to `SIM=questa`, but can be overridden on the command line by
e.g. running `make SIM=ius` or `make SIM=xcelium` (to get the Cadence
Xcelium simulator).

The tests can be ran in passthrough mode or normal mode; simply run
```make PASSTHROUGH=1``` to remove the FIFO and run in passthrough mode.

To run with the graphical user interface (to look at waveforms, etc.)
run ```make GUI=1```. For other options that can be passed to the tests,
a list of environment variables respected by cocotb can be found in the
[cocotb documentation](https://docs.cocotb.org/en/stable/building.html).

## Useful Notes

These tests are probably not complete, and there is more work that could be
done here. The following notes might be useful if you are planning to do some
work on the spy buffer, or on its tests.

### Writing More Tests

There are probably edge cases that are not covered by the existing tests;
I've only tried to verify that basic functionality works for now. If you'd
like to write more tests, take a look at [SpyBufferTests.py](./SpyBufferTests.py),
which contains the Python code for each test. Much of the test code, for low-level
tasks such as communicating with the FIFO or reading events out of the spy
memory, can be found in the [testbench](./testbench) directory.

### Post-Synthesis Simulations

At the moment, these tests are not set up to run post-synthesis simulations;
only RTL simulations. It is possible to use cocotb to run pre- and post-
synthesis simulations; the spy buffer unit tests have been set up with a
[RTL wrapper file](./SpyBufferWrapper.v) that instantiates the spy buffer
object to enable exactly this. Both simulations can use the wrapper as the
design under test (DUT), and then either load the RTL implementation (for RTL
simulations) or the post-synthesis netlist (for post-synthesis simulations);
this just requires some extra logic in the Makefile.

To do this, we'd also have to set up a script (maybe using hog? or just TCL?)
to synthesize just the spy buffer itself. That would also enable continuous
integration for both the synthesis and verification.

# Spy Buffer Interface Documentation

This section contains a description of the spy buffer interface, as implemented
in these tests. It should likely be moved elsewhere eventually, perhaps to a
dedicated "doc" directory (and perhaps, if we use doxygen or a similar tool
to automatically produce API documentation, into the source code itself).

The spy buffer consists of three main components: the internal spy controller,
which handles writes and reads from the spy memory, the playback controller,
which handles reads from (and writes to) the spy memory in playback mode, and
the (optional) FIFO, which provides flow control functionality.


## Flow Control

By default, the spy buffer contains a dual-clock FIFO, which can provide
flow control between two blocks. The width of this FIFO can be controlled via
the parameter ```FC_FIFO_WIDTH```. The FIFO can optionally provide a clock
domain crossing between the write-side and read-side block; to that end,
the spy buffer takes two clocks (and two resets) as inputs. The FIFO has
two status outputs, "almost full" and empty", for the write and read side
respectively, to enable flow control.

Write and read enables can be set high by user on either the write- or read-
side when writing data in or reading data out. The width of the data can be
set by the parameter ```DATA_WIDTH```; this is presumed to include one extra bit
indicating whether a data word is a metadata word or not. (The default value
of 65 means that the spy buffer will expect 64+1-bit words).

The ```freeze``` input controls whether or not data words are copied to the
spy buffer; if freeze is set low, and if the write enable is set high, on the
rising edge of the write clock, words will be copied into the spy memory. (See
the next section for more details about access to the spy memory).

The FIFO can be removed by instantiating the spy buffer with the parameter
```PASSTHROUGH``` set to 1. In passthrough mode, the FIFO is not created,
and no flow control is provided. Instead:

* The data input will be assigned continously to the data output.
* The NOT of the write enable is assigned to the empty flag, thus turning the
write enable into a valid flag. (If the write enable is fixed to 1, the data is always
assumed to be valid).
* On the rising edge of the write clock, if the write enable is high, the
data input is copied into the spy memory.
* The read clock is ignored.
* The read enable is ignored, and the "almost full" flag set to zero (meaning
writes are never blocked).

It would be possible to also connect the read enable to the "almost full" flag,
to provide for backpressure. At the moment, this has **not** been implemented (it
would require some passthrough-specific changes to the testbench to work
properly).

## Spy Access

As long as the spy buffer is not frozen, input data words will be copied
into the large spy memory by the internal spy controller. The spy memory is
intended to be implemented as block-RAM (or Xilinx "UltraRAM" on the TP); its
size is controlled by the ```SPY_MEM_WIDTH``` parameter and its width by the
```DATA_WIDTH``` parameter already detailed above.

The spy buffer also contains a second, smaller memory (which uses the same
RTL implementation, and so could also be implemented by block RAM), referred
to as the "event list" (or "metalist"). When the internal spy controller sees
a metadata word (with the MSB metadata bit set), it then checks the eight
next-most-significant bits to see if the word is a start-of-event word.
If it is, then in addition to copying the word into the spy memory, then the
address of the newly-written word is written into the event list. This enables
a quick way for the user to find the boundaries of events in the spy memory.

The event list's size is controlled by the parameter ```EL_MEM_WIDTH```. (The
default values of all the sizes and widths are not necessarily known to be
reasonable at the moment; they are intended to be adjusted by the user for
specific use cases).

To keep the two memories synchronized, whenever the spy memory's write pointer
"loops" back to zero, a sentinel word is inserted in the event list. Sentinel
words can be distinguished from normal words by their most significant bit;
if the MSB is one, then the word merely indicates that the spy memory looped
and not the presence of a start-of-event word. (To support these sentinel words,
entries in the event list are one bit *larger* than the width of the spy memory).

The sentinel information is intended to be used during readout. To read out
the spy buffer, and to read out all events:

* Set the ```freeze``` input high. This stops all writes into the spy memory,
but does not stop data from flowing between the two connected blocks.
* Read the event list, starting backwards from the newest entry until
two sentinel words have been read (or until the entire thing has been read out).
Any older entry in the event list points to an event that has been overwritten
in the spy memory, so can safely be ignored.
* Check the current write address in the spy memory. Any event older than the
first sentinel word, and with an address below the spy memory's current address,
will also have been overwritten-- so these events can be dropped as well.
* The remaining values from the event list should point to valid events, which
can then be read.

At present, communication with the spy memory is relatively simple. Two sets
of signals are provided; those beginning with ```spy_meta``` are for the
event list and those just with ```spy_``` (no meta) are for the main spy memory.
Each signal is described below for the main memory, but the descriptions are
identical for the event list versions.

* ```spy_write_addr```: Output. The current write address in the memory. Since
the spy memory is a circular buffer, this is a pointer to the *oldest* entry in
the memory. (The newest entry will then be ```spy_write_addr``` - 1).
* ```spy_read_addr```: Input. The contents of this address will be read out
if the read enable is set high.
* ```spy_read_enable```: Input. Set high to read out the contents of the requested
read address.
* ```spy_data```: Output; the data read out from the memory a clock after setting
the requested address and setting the read enable high.

At present, this is all done in the FIFO's write clock domain. In the future,
this access will likely be moved to its own clock domain.

## Playback

The spy buffer also supports a playback mode, where the current contents of the
spy memory can be read back into the connected block. In playback mode, all
communication with the input/write-side block is ignored, and new words are
not written into the spy memory. Instead, the playback controller reads words
out of the spy memory and generates write enables for the FIFO (if necessary).

While in theory, playback could be used to play back the current contents of
the spy buffer, this is not recommended. Playback will begin from address 0,
which (after normal operations) may or may not correspond to a valid start
of event word. Instead, the intended workflow is to:

* Put the spy buffer into "playback write" mode, where the user can write
directly into the spy memory's RAM.
* Reset the spy memory to clear all read/write pointers, and FIFO/RAM
occupancy.
* Write a series of words into the spy memory.
* Once all data that you would like to send has been written, put the spy
buffer into "playback once" or "playback loop" mode. In these modes, the
playback controller will read from address 0 until they reach the current
write address in the spy memory (so you can play back as many or as few
words as desired).
* Once reaching the spy memory, in the "playback once" state, playback will
halt and the spy buffer will do nothing until the state is toggled (or
until it is reset). In the "playback loop" state, the spy buffer will instead
jump back to address zero and begin the playback again from the beginning;
this will continue indefinitely until the state is toggled or the block reset.

The playback state is controlled by a 2-bit toggle, ```playback```. The supported
states described above have the following values:

* 0: ```NO_PLAYBACK```: normal operations. The spy buffer **must** be initialized
with this value set, otherwise the playback controller will stop both normal operations
and playback.
* 1: ```PLAYBACK_ONCE```: play back data one time, then stop.
* 2: ```PLAYBACK_LOOP:``` play back data continuously.
* 3: ```PLAYBACK_WRITE```: enable writes to the spy memory.

Writes to the spy memory are controlled via two top-level inputs; ```playback_write_enable```
and ```playback_write_data```. Both are only functional in playback write mode.

Playback will work whether or not the FIFO is present; in non-passthrough
mode, the playback controller will automatically generate write enables
and temporarily pause if the FIFO becomes full. In passthrough mode, it will
instead read continuously.
