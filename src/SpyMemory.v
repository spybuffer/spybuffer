// A parameterized, inferable, true dual-port, dual-clock block RAM in Verilog.
 //https://danstrother.com/2010/09/11/inferring-rams-in-fpgas/

module SpyMemory_tdp #(
		       parameter DATA_A  = 72, //65
		       parameter ADDR_A  = 10,
		       parameter BYTES_A = 9,
		       parameter DATA_B  = 32,
		       parameter ADDR_B  = 11,
		       parameter BYTES_B = 4
		  
) (
    // Port A
   input wire 		   a_clk,
   input wire 		   a_wr,
   input wire [ADDR_A-1:0] a_addr,
   input wire [DATA_A-1:0] a_din,
   output reg [DATA_A-1:0] a_dout,
   input 		   a_en,
     
    // Port B
   input wire 		   b_clk,
   input wire 		   b_wr,
   input wire [ADDR_B-1:0] b_addr,
   input wire [DATA_B-1:0] b_din,
   output reg [DATA_B-1:0] b_dout,
   input 		   b_en
);
 
   localparam ADDR_A_BYTES = $clog2(BYTES_A);
   localparam ADDR_B_BYTES = $clog2(BYTES_B);   
   
// Shared memory
   reg [7:0] 		  mem [(BYTES_A * (2**ADDR_A))-1:0];
//reg [DATA_A-1:0] mem [(2**ADDR_A)-1:0];

   wire [ADDR_A + ADDR_A_BYTES -1:0] a_addr_bytes;   
   wire [ADDR_B + ADDR_B_BYTES -1:0] b_addr_bytes;

   assign a_addr_bytes = a_addr  * BYTES_A;
   assign b_addr_bytes = b_addr  * BYTES_B;

   // Port A
always @(posedge a_clk) begin
   if(a_en) begin
      for(integer i=0; i<BYTES_A; i=i+1)
	begin	
	   if(a_wr) begin
              a_dout[i*8 +: 8]        <= a_din[i*8 +: 8];
              mem[a_addr_bytes + i] <= a_din[8*i +: 8];
	   end 
	   else begin
	      a_dout[8*i +: 8]      <= mem[a_addr_bytes + i];
	   end
	end
   end
end

   // Port B
always @(posedge b_clk) begin
   if(b_en) begin
      for(integer i=0; i<BYTES_B; i=i+1)
	begin
	   
	   if(b_wr) begin
	      b_dout[8*i +: 8]        <= b_din[8*i +: 8];
	      mem[b_addr_bytes + i] <= b_din[8*i +: 8];
	   end
	   else begin
	      b_dout[8*i +: 8]      <= mem[b_addr_bytes + i];
	   end
	end // for (integer i=0; i<BYTES_B; i=i+1)
   end
end // always @ (posedge b_clk)
   

 
endmodule
