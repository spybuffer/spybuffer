// Spy playback controller block.
// When in playback mode, the spy buffer can read its contents into
// the associated flow control FIFO. This allows the spy buffer to be
// "played back" (ideally, after a block write to put known data into it).

// This block generates the relevant control signals for the spy buffer
// and spy FIFO to make this happen. Logic in SpyBuffer.v uses the
// outputs of this block

// Ben Rosser <bjr@sas.upenn.edu>

`default_nettype none

  module SpyPlayback #(
		      
		       
		       // Width of the spy buffer's spy memory. This is also the width of addresess.
		       parameter MEMWIDTH_A = 6,
		       parameter MEMWIDTH_B = 6,
		       parameter SPY_WORDS_IN_DATAFORMAT = 4
		       ) (
			  input wire 		      clock,
			  input wire 		      spy_clock,
			  input wire 		      resetbar,
			  
    // Is the flow control FIFO full?
			  input wire 		      full,

    // Are we in playback mode?
			  input wire [1:0] 	      playback,

    // Current write address in spy memory.
			  input wire [MEMWIDTH_B-1:0] playback_spy_mem_wptr, //Clock Domain : spy_clock

  
    // Outputs: control signals for FIFO.
			  output reg 		      playback_enable, //Clock Domain : clock



    // Outputs: control signal for spy memory.
			  output reg [MEMWIDTH_A-1:0] playback_spy_read_addr,
			  output reg 		      playback_read
			  
);

    // Initial playback address. This should likely be zero, but expose as parameter anyway.
    // Actually, it needs to be... well, this.
   localparam INITIAL_ADDR = 2**MEMWIDTH_A - 1;
   localparam ADDR_SHIFT   = $clog2(SPY_WORDS_IN_DATAFORMAT);
   
   // Include the SpyProtocol.vh file.
`include "SpyProtocol.vh"
   
   // It's been a while; I'm somewhat worried about the sequencing here.
   
   // We need a bit of internal state to know that we're actually running--
   // due to some synchronization issues around the first (and last!) word.
   // The edge case of "what if the write pointer == 0" makes it hard to do
   // this otherwise.
   reg 						      running = 0;
   reg [MEMWIDTH_B-1:0] 			      playback_spy_mem_wptr_clock_d, playback_spy_mem_wptr_clock_2d;
   reg [1:0] 					      playback_d,playback_2d;
   reg 						      run_playback_once, run_playback_once_d;
   reg 						      pb_run;
   reg [2:0] 					      playback_state;
   reg [3:0] 					      playback_enable_d;
   
   reg [ADDR_SHIFT-1:0] 			      residue;
   reg 						      flush_done;
		      
   reg 						      full_d;
 
   
   
   always @(posedge clock) begin
      if(!resetbar)
	begin
	   playback_spy_mem_wptr_clock_d <= 0;
	   playback_spy_mem_wptr_clock_2d <= 0;
	   residue                                             <= 0;

	end
      else
	begin
	   playback_spy_mem_wptr_clock_d <= playback_spy_mem_wptr >> ADDR_SHIFT;
	   
	   playback_spy_mem_wptr_clock_2d <= playback_spy_mem_wptr_clock_d;
	   residue                                             <= playback_spy_mem_wptr[ADDR_SHIFT-1:0];
	   
	end
   end

   //Move playback to correct clock domain
   always @(posedge clock) 
     begin
	if(!resetbar)
	  begin
	     playback_d    <= 2'b0;
	     playback_2d   <= 2'b0;
	     //playback_read <= 1'b0;
	     run_playback_once <= 1'b0;
	     run_playback_once_d <= 1'b0;
	     
	  end
	else
	  begin
	     playback_d    <= playback;
	     playback_2d   <= playback_d;
	     run_playback_once_d <= run_playback_once;
	     
	     run_playback_once <= ((playback_d == START_PLAYBACK_ONCE) && (playback_2d != START_PLAYBACK_ONCE));

	     
	     //playback_read <= (playback_2d == PLAYBACK_ONCE) || (playback_2d == PLAYBACK_LOOP);
	     //playback_read <= (run_playback_once) || (playback_2d == PLAYBACK_LOOP);	   
	  end
     end
   
    always @(posedge clock) begin
        // If we're in a resetbar state, OR if we're not in playback mode,
        // reset internal state (the playback address to 0).
       if (!resetbar) begin
          // resetbar playback and spy signals.
          playback_spy_read_addr <= INITIAL_ADDR;
          running                            <= 0;
	  pb_run                              <= 0;
	  playback_state                 <= NO_PLAYBACK;
	  playback_read                  <= 0;
	  flush_done                        <= 0;
	  
        end else begin
            // How playback works depends on which mode we're in. Currently there are two.
            case (playback_state) //{pb_run,playback_2d})
	      NO_PLAYBACK:
		begin
		   playback_spy_read_addr <= INITIAL_ADDR;
		   running                            <= 0;
		   pb_run                              <= 0;
		   if(run_playback_once)
		     begin
			playback_state           <= RUN_PLAYBACK_ONCE;
		     end
		   else
		     begin
			playback_state           <= (playback_2d != START_PLAYBACK_ONCE)? {1'b0, playback_2d} : NO_PLAYBACK;
		     end
		       
		   //playback_state                 <= {1'b0, playback_2d};		
		   playback_read                  <= 0;
		   flush_done                        <= 0;
		end
                // In the stop state, loop through the memory once, from beginning to end,
                // and then stop. Wait for the external controller to reset.
              START_PLAYBACK_ONCE: begin
		 //playback_state <= (run_playback_once)? RUN_PLAYBACK_ONCE : START_PLAYBACK_ONCE;
		 playback_state <= (run_playback_once)? RUN_PLAYBACK_ONCE : ((playback_2d == START_PLAYBACK_ONCE)?  NO_PLAYBACK : START_PLAYBACK_ONCE);
	      end
	      RUN_PLAYBACK_ONCE: begin              
                 if (full == 0 ) begin
                       // We run until we hit the spy write address, at which point
                       // we are about to stop. At that point, set the running flag
                       // and stop!
		  
		    if(flush_done == 1)
		      begin
			 running <= 0;			    
			 playback_state <= NO_PLAYBACK;
			 playback_read   <= 1'b0;
		      end
                    if (running && playback_spy_read_addr == playback_spy_mem_wptr_clock_2d) begin //spy_write_addr) begin
		       if(playback_enable == 0 || full_d)begin //wait for FIFO to be flushed
			  playback_state <= RUN_PLAYBACK_ONCE;
		       
		    end
		      else  if(residue == {ADDR_SHIFT{1'b1}} )
			 begin 	      
			    running <= 0;			    
			    playback_state <= NO_PLAYBACK;
			    playback_read   <= 1'b0;
			 end
		       else
			 begin			    
			    flush_done  <= 1;
			    running       <= 1;
			    playback_read   <= 1'b1;
			    playback_spy_read_addr <= playback_spy_read_addr + 1;
			 end
		    end else begin
		       running       <= 1;
		       playback_read   <= 1'b1;
                       playback_spy_read_addr <= playback_spy_read_addr + 1;
		       	       		     
		    end // else: !if(running && playback_spy_read_addr == playback_spy_mem_wptr_clock_2d)
		    
                 end // if (full == 0)
			 
              end // case: RUN_PLAYBACK_ONCE
	      
	      
                // In the loop state, loop through the memory continuously.
                // Note: we don't allow a different stop address. I think that's okay
                // as a prototype?
                PLAYBACK_LOOP: begin
		   playback_state                 <=  (run_playback_once)? RUN_PLAYBACK_ONCE : {run_playback_once, playback_2d};		   
		   playback_read   <= 1'b1;
                   if (full==0) begin
                      // We run until we are about to hit the spy write address,
                      // at which point we loop back to address zero but otherwise keep going.
                      running <= 1;
		      if (running && playback_spy_read_addr  == playback_spy_mem_wptr_clock_2d) begin
//		      if (running && playback_spy_read_addr  == INITIAL_ADDR) begin
                         playback_spy_read_addr <= 0;
                      end else begin
                         playback_spy_read_addr <= playback_spy_read_addr + 1;
                      end
                   end
                end

                // In this mode: do nothing. This mode stops reads into the FIFO
                // and stops the spy decision block from writing, while allowing the user
                // to write into the spy memory.
                PLAYBACK_WRITE: begin
		   playback_state                 <=  {run_playback_once, playback_2d};		   		   
                   playback_spy_read_addr <= INITIAL_ADDR;
		   playback_read                  <= 1'b0;
		   flush_done                        <= 0;
		   
                    // TODO support clock domain crossing for these signals!
                    // Not sure if that should live in this module or in SpyBuffer.v.              
                    running <= 0;
                end

                // Do nothing in the (hopefully unreachable!) default case.
                default: begin
                   playback_spy_read_addr <= INITIAL_ADDR;
		   playback_state                 <= {run_playback_once, playback_2d};		   
                   running <= 0;
                end

            endcase
        end
    end // always @ (posedge clock)

   //Move clock domain for playback signal

    // Second always block: control the playback state itself.
    // I separated out this logic just for readability. The playback runs
    // one clock behind reading from the spy memory, so we introduce a register
    // (flip-flop) that I called "running" to implement the delay in starting up.
    always @(posedge clock) begin
        if (!resetbar) begin
            playback_enable   <= 0;
	   playback_enable_d <= 0;
	   full_d                       <= 0;
	   
        end else begin
	   
	   full_d                               <= full;
	   
            case (playback)
                // Playback once state.
                START_PLAYBACK_ONCE: begin
                    //if (running && !full) begin
		   

		   if(full || full_d)
		     begin
			playback_enable        <= playback_enable_d[0];
//			playback_enable_d[0] <= 0;
			playback_enable_d[0] <= ~full & full_d; //0;
			playback_enable_d[1] <= ~full & full_d; //0;
			
			playback_enable_d[3:2] <= playback_enable_d[2:1];
		     end
		   else
		     begin
			playback_enable_d[3:1] <= playback_enable_d[2:0];
			 playback_enable <= playback_enable_d[1];
			if (run_playback_once || run_playback_once_d ||  (playback_spy_read_addr < playback_spy_mem_wptr_clock_2d-1) )begin		   
                           playback_enable_d[0] <= 1;
			end else begin
                           playback_enable_d[0] <= 0; //~full & full_d; //0;
			end
		     end
                end
	      // Playback loop state.
                // Not sure if Verilog supports C-style fallthrough here;
                // the above ended up being identical.
              PLAYBACK_LOOP:
		begin	    
                   if (running && !full) begin
                      playback_enable <= 1;
                   end else begin
                      playback_enable <= 0;
                   end
		end	    
                default: begin
                    playback_enable <= 0;
                end
            endcase
        end
    end

endmodule
`default_nettype wire
