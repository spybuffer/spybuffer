/*
 * SpyBufferWrapper.v
 *
 * begun 2019-11-11 by Bill Ashmanskas
 * adjusted 2020-01-27 by Ben Rosser.
 */

`timescale 1ns / 1ps
`default_nettype none

  module SpyBufferWrapper #(
			    // inclusive of metadata flag (bit 64)
			    parameter FLOW_DWIDTH = 65, //65, 65 is not supported configuration, so needs to be 128
			    parameter SPY_DWIDTH = 32, 
			    // Width of the event list-- needed for addresses.
			    parameter EL_MEM_WIDTH = 7, //4,
			    // Width of the spy memory itself-- also needed for addresses.
			    parameter SPY_MEM_ADDR_WIDTH_A = 7,
			    parameter SPY_MEM_ADDR_WIDTH_B = 9			   
			    ) (
			       // main TP clock, nominally 200 MHz
			       input wire 		    clock,
			       input wire 		    reset,
			       input wire [FLOW_DWIDTH-1:0] data_in,
			       input wire 		    freeze,
			       input wire [1:0] 	    playback,
			       output wire [SPY_DWIDTH-1:0] data_out,
			       output wire 		    afull,
			       output wire 		    empty,
   
			       input wire 		    spy_clock
			       );
   
   // A tad awkward; it'd be nice to set PASSTHROUGH directly
   // from the makefile. I'm sure it can be done, but it might be
   // simulator specific?
`ifdef PASSTHROUGH
   localparam PASSTHROUGH = 1;
`else
   localparam PASSTHROUGH = 0;
`endif

   //BRAM_SIZE_A is actual port width of Spy Memory. User would have to pick a size that is supported by FPGA architecture.
   localparam BRAM_SIZE_A             = 128;  //65, 65 is not supported configuration, so needs to be 128
   localparam INPUT_ZERO_PADDING      = BRAM_SIZE_A - FLOW_DWIDTH;
   localparam SPY_META_DATA_WIDTH     = (SPY_MEM_ADDR_WIDTH_A + 1); //add sentinel bit
   
     
   // Random wires for the spy buffers.
   // I wrote the testbench in such a way that none of these matter;
   // the driver/monitor reaches into the spy buffer's toplevel
   // rather than look at these. But maybe that was a bad idea.
   
   // These are inputs, and should get zero'd.
   wire 						we;
   wire 						re;
   
   // These are the "meta" (event list) controls.
   wire [EL_MEM_WIDTH-1:0] 				meta_write_ptr;
   wire [EL_MEM_WIDTH-1:0] 				meta_read_ptr;
   // The spy meta data has a sentinel bit, indicating wrap-around.
   wire [SPY_META_DATA_WIDTH-1:0] 			meta_data;
   
   
   // These are the spy buffer controls for actual data words.
   wire [SPY_MEM_ADDR_WIDTH_B-1:0] 			spy_write_ptr;
   wire [SPY_MEM_ADDR_WIDTH_B-1:0] 			spy_read_ptr;
   wire [SPY_DWIDTH-1:0] 				spy_data;
   wire 						spy_re;
   
   // These are the spy buffer controls for writing into the
   // buffer for playback.
   wire [SPY_DWIDTH-1:0] 				playback_write_data;
   wire 						playback_we;
   wire [INPUT_ZERO_PADDING-1:0] 			zp;

   assign zp = 0;
   
   // Create an input spy buffer.
   // NOTE: size is exclusive of metadata flag bit here.
   SpyBuffer #(
	       .DATA_WIDTH_A(BRAM_SIZE_A),
	       .DATA_WIDTH_B(SPY_DWIDTH),
	       .EL_MEM_WIDTH(EL_MEM_WIDTH),
	       .SPY_MEM_WIDTH_A(SPY_MEM_ADDR_WIDTH_A),
	       .SPY_MEM_WIDTH_B(SPY_MEM_ADDR_WIDTH_B),
	       .PASSTHROUGH(PASSTHROUGH),
	       .SPY_META_DATA_WIDTH(SPY_META_DATA_WIDTH)
	       ) spy_buffer (
			     .rclock(clock),
			     .wclock(clock),
			     .rresetbar(reset),
			     .wresetbar(reset),
			     .write_data({zp,data_in}),
			     .write_enable(we),
			     .read_data(data_out),
			     //.read_enable(~empty &  re), //Use re to control read_enable from SpyBufferTests.py
			     .read_enable(~empty ),
			     .almost_full(afull),
			     .empty(empty),
			     // The following should not be needed until one actually wants
			     // to use the spy-buffer functionality, whereas for now we just
			     // want to use the fifo functionality.
			     .freeze(freeze),
			     .spy_en(1'b1),
			     .spy_addr(spy_read_ptr),
			     .spy_meta_en(1'b1),
			     .spy_meta_addr(meta_read_ptr),
			     .spy_meta_read_data(),
			     .spy_meta_write_data('b0),
			     .spy_meta_wen(1'b0),
			     .dbg_spy_write_addr(spy_write_ptr),
			     .dbg_spy_meta_write_addr(meta_write_ptr),
			     .spy_data(spy_data),
			     .dbg_spy_meta_read_data(meta_data),
			     .playback(playback),
			     .spy_write_enable(playback_we),
			     .spy_write_data(playback_write_data),
			     .spy_clock(spy_clock),
			     .spy_clock_meta(spy_clock)
			     );
   
   
   initial
     begin
	$dumpfile("sim_build/spybuffer.vcd");
	$dumpvars(0,SpyBufferWrapper);
	//	$dumplimit(2000);
	
     end
endmodule

`default_nettype wire
